# Highway Hunter: Breakdown

A decoding and analysis of the `1.DAT` file from 1994 DOS game, Highway Hunter (shareware version). The full game uses `123.DAT` with the same format.

## Data Archive (`1.DAT`)
Consists of 2 parts, an Index and the Contents

### Index

Begins with a 16-bit LSB-first value indicating the size of the Index.
Index Entries consist of:
 * 32-bit LSB-first value indicating the file position in the DAT
 * 12-byte (+ NULL) string of the file name
Any excess space between records and end of Index is null-filled.

### Files

Begins with  32-bit LSB-first value indicating the size of the file (not including this field).
Followed by the file data.

## Files Stored in Data Archive

The Data Archive stores the resoures used by High Hunter.
Unfortunatly it appears a large amount of game data is part of the code, not resources.


### SpriteSheets (`*.spr`)

Sprite:
2 bytes (LSB) - width
2 bytes (LSB) - height
width * height bytes row-major color indices (value of 0 is transparent)

Spritesheets are multiple sprites packed with no header or separators

### Maps (`*.map`)

6 byte records for entity info
```
{
0-1: Horizontal Position 0-320 - LSB,  Relative to screen not road
2:Entity Type - This changes both sprite & behavior (Examples from Urbane, each map seems different)
   0: Sprite 2-3
   1: Sprite 0-1
   2: Sprite 37 & 38
   3: Sprite 9-11
   4: Sprite 13 (fires Sprite 39)
   5: Bonus Spirte 0-15 "U"
   6: Bonus Sprite 32-47 "C"
   7: Bonus Sprite 16-31 "0" ("O"?)
   8: Bonus Sprite 50-51 "Spinning Shield"
   9: Bonus Sprite 51-53 "Side Shield"
   A: Bonus Sprite 54-55 "Front Sheild"
   B: Sprite 12 (fires Sprite 39)
   C: Sprite 31-33 & 34 (Fires 30 2x) "Boss"
   D: Sprite 31-33 (Bottom Part of Boss)
  *E: Sprite  30-31 (Green lazer and 2x 1st frame of boss bottom)
  *F: Sprite  30-31 (Green lazer and 2x 1st frame of boss bottom)
 *10: Sprite  30-31 (Green lazer and 2x 1st frame of boss bottom)
  15: Bonus Sprite 48-49 "Extra Life"
3: ? (Probably the upper byte of a 16-bit Entity Type)
4-5: LSB, Distance on road, from the end, capped at max of 0x4C9B, Entities always placed at a increment of 5px (round up), with minimum of 5px separation
    This gives a maximum of 0xF53 or 3923 rows of placement
}
```

Not contained in th map file, and possibly hard-coded:
 * Road path
 * Background entities
  - Shooting towers (start ~1/2 way into Urbane)
   * Urbane Sprites 17 & 18 (Fire Sprites 29-19)
  - Background Traffic
   * Urbane Sprite 16

### Bitmaps (`*.lbm`)
320x200px IFF-conforming Interleaved Bitmaps with 256-color palette & color cycling data

None of the ranges specify a rate or time, they are like spciifying which ranges should be used for certain things.
Most roads seem to contain an extra CRNG Chunk that the backgrounds do not, this range *does* contain a rate, and has information in the padding bytes.
Suspicion—the ranges correspond with the colors used in Sprites.

### Animation (`*.ani`)
Animations played on top of the full-screen bitmaps.
Haven't looked into format yet, but not recognized by VLC.

### Color Palettes? (`*.clr`)
Possibly color palettes, 128 byte files, with 7-bit data. Bitmaps appear to provide all the color pallete information needed for normal rendering of sprites. These files are all named "trans*n*.clr", so possibly used during transistions between bitmaps.

### Font (`*.fnt`)
Unknown font format, assuming it is a font.

### Music (`*.cmf`)
Presumably Creative Music Format, haven't confirmed

### Sound Effects (`*.voc`)
Create Voice file. Several appear to have no sound in VLC, so fomat *may* be non-standard.

### Title Screen Demo (`demo.mac`)
Macro file describing demo player's movements, somehow.
