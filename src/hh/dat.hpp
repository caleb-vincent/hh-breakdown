#pragma once
//
// Copyright (c) 2021 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include <filesystem>
#include <stdint.h>
#include <string>
#include <vector>

namespace hh
{
   struct File
   {
      std::string name;
      std::vector<uint8_t> data;
   };


   class Dat
   {
   public:
      // The DAT file has some padding after the index, this will be needed to save with identical layouts
      uint16_t fileStart{ 0 };

      // The rest is just wrapper around a vector. . .

      auto& operator[](size_t const index)
      {
         return m_files[index];
      }

      auto const& operator[](size_t const index) const
      {
         return m_files[index];
      }

      auto& back()
      {
         return m_files.back();
      }

      auto const& back() const
      {
         return m_files.back();
      }

      auto begin()
      {
         return m_files.begin();
      }


      auto begin() const
      {
         return m_files.begin();
      }

      template<typename ...Args >
      auto& emplace_back(Args ...args)
      {
         return m_files.emplace_back(std::forward(args)...);
      }

      auto end()
      {
         return m_files.end();
      }

      auto end() const
      {
         return m_files.end();
      }

      void resize(size_t const newSize)
      {
         m_files.resize(newSize);
      }

      void reserve(size_t const size)
      {
         m_files.reserve(size);
      }

      auto size() const
      {
         return m_files.size();
      }

   private:
      std::vector<File> m_files;
   };

   Dat readFile(std::filesystem::path const& filename);
   void writeFile(std::filesystem::path const& filename, Dat const& dat);
}
