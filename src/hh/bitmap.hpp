#pragma once
//
// Copyright (c) 2021 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "dat.hpp"

#include "resources.hpp"

#include <palletone/PalettedImage.h>

#include <SFML/Graphics/Texture.hpp>

namespace hh
{

   class Bitmap
   {
   public:
      PalettedImage image;
      sf::Texture   texture;
      sf::Texture   palette;

      Bitmap( PalettedImage img, sf::Texture tex, sf::Texture pal);
   private:
      friend std::unique_ptr<Bitmap> CreateResource<Bitmap>(File const& datFile);


      Bitmap(Bitmap const&) = delete;
      Bitmap& operator=(Bitmap const&) = delete;
   };


   template<>
   std::unique_ptr<Bitmap> CreateResource<Bitmap>(File const& datFile);

}
