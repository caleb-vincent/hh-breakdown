#pragma once
//
// Copyright (c) 2021 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "dat.hpp"

#include "color_palette.hpp"
#include "resources.hpp"

#include <SFML/Graphics/Texture.hpp>

#include <utility>

/******************************************************************************
*  Sprite sheet files are a sequence of sprites.
*  No padding, alignment or dividers
*
*  SpriteData
*  {
*     width    : UINT16, LSB
*     height   : UINT16, LSB
*     data     : BYTE[width * height]
*  }
*
*  FileData
*  {
*     sprites : Sprite[]
*  }
******************************************************************************/

//!
namespace hh
{

   class SpriteSheet
   {
   public:
      SpriteSheet(File const& datFile);

      sf::Texture const& operator[](size_t i) const;

      size_t Size() const
      { return m_sprites.size(); }

   private:
      friend std::unique_ptr<SpriteSheet> CreateResource<SpriteSheet>(File const& datFile);

      SpriteSheet(SpriteSheet const&) = delete;
      SpriteSheet& operator=(SpriteSheet const&);

      void GenerateSprites() const;

      File const& m_datFile;

      // For the time being the textures and palette versions are considered the presentation of
      //    the spite, not the sprite itself, hence the mutable. This allows const access methods since
      //    the sprite's data is not actually getting modified.
      mutable std::vector<sf::Texture> m_sprites;
      mutable int64_t m_paletteVersion{ -1 };
   };

   template<>
   std::unique_ptr<SpriteSheet> CreateResource<SpriteSheet>(File const& datFile);
}
