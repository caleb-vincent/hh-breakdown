//
// Copyright (c) 2021 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "dat.hpp"

#include <algorithm>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <iterator>
#include <numeric>
#include <vector>


namespace fs = std::filesystem;

namespace
{
   auto& info = std::cout;
   //auto& warn = std::clog;
   auto& error = std::cerr;
}

using namespace hh;

//! @todo clean up the type used in reading the DAT file

static constexpr auto FILE_NAME_LENGTH = 13;

// Data Types used in the dat file
using EndPosition_t = uint16_t;
using FilePosition_t = uint32_t;
using FileName_t = char[FILE_NAME_LENGTH];

namespace
{
   struct IndexEntry
   {
      FilePosition_t filePosition;
      char     fileName[13];
   };

   struct Index
   {
      EndPosition_t    endPosition;
      std::vector<IndexEntry> entries;
   };
}


std::ifstream& operator>>(std::ifstream& datFile, IndexEntry& entry)
{
   datFile.read(reinterpret_cast<char*>(&entry.filePosition), sizeof(entry.filePosition));
   datFile.read(entry.fileName, sizeof(entry.fileName));
   info << "File '"<< entry.fileName << "' at 0x" << std::hex << entry.filePosition << "\n";
   return datFile;
}


std::ifstream& operator>>(std::ifstream& datFile, File& file)
{
   uint32_t fileSize{0};
   datFile.read(reinterpret_cast<char*>(&fileSize), sizeof(fileSize));
   file.data.reserve(fileSize);
   file.data.insert(file.data.begin(), fileSize, 0);
   datFile.read(reinterpret_cast<char*>(file.data.data()), file.data.size());
   info << "Read " << file.name << " " << std::hex << file.data.size() << " bytes\n";
   return datFile;
}


std::ifstream& operator>>(std::ifstream& datFile, Index& index)
{
   datFile.read(reinterpret_cast<char*>(&index.endPosition), sizeof(EndPosition_t));
   info << "Index Size: " << index.endPosition + 1 << "\n";
   auto const entryCount = (index.endPosition - sizeof(EndPosition_t)) / (sizeof(FilePosition_t) + sizeof(FileName_t));
   info << "Index entries: " << entryCount << "\n";
   index.entries.reserve(entryCount);
   for(auto i{0u}; i < entryCount; ++i)
   {
      index.entries.emplace_back();
      datFile >> index.entries.back();

   }
   datFile.seekg(index.endPosition);
   return datFile;
}


Dat hh::readFile(std::filesystem::path const& filename)
{
   Dat dat;
   std::ifstream datFile(filename, std::ios::binary);
   if(datFile.good())
   {
      Index index;
      datFile >> index;
      dat.fileStart = index.endPosition + 1;
      dat.reserve(index.entries.size());
      for(auto& fileIndexEntry : index.entries)
      {
         dat.emplace_back();
         datFile.seekg(fileIndexEntry.filePosition);
         dat.back().name = fileIndexEntry.fileName;
         datFile >> dat.back();
      }
      datFile.close();

   }
   else
   {
      error << "Invalid DAT file\n";
      exit(1);
   }
   return dat;
}


void hh::writeFile(std::filesystem::path const& filename, Dat const& dat)
{
   std::ofstream datFile(filename, std::ios::binary);
   if(datFile.good())
   {
      // make sure we start the files at the same position as the original, unless the index grew
      auto const indexSize{ std::max<EndPosition_t>(dat.size() * (FILE_NAME_LENGTH + sizeof(FilePosition_t)), dat.fileStart - 1) };
      datFile.write(reinterpret_cast<char const*>(&indexSize), sizeof(indexSize));

      // The starting point of the file storage after the index also has to offset by the index size size
      auto offset{ static_cast<FilePosition_t>(indexSize + sizeof(EndPosition_t)) };

      // write out the file names and offsets
      for(auto const& file : dat)
      {
         char constexpr BLANK_FILENAME[FILE_NAME_LENGTH]{ 0 };

         datFile.write(reinterpret_cast<char const*>(&offset), sizeof(offset));
         datFile.write(file.name.c_str(), file.name.size());
         datFile.write(BLANK_FILENAME, FILE_NAME_LENGTH - file.name.size()); // fill in after the file name with nulls to a fixed length
         offset += file.data.size() + sizeof(FilePosition_t);
      }

      // restore any padding the original had between index and files
      std::vector<char> fill((indexSize + sizeof(indexSize)) - datFile.tellp(), '\0');
      datFile.write(fill.data(), fill.size());

      // write out the file sizes and data, no padding needed
      for(auto const& file : dat)
      {
         auto const fileSize{ static_cast<FilePosition_t>(file.data.size()) };
         datFile.write(reinterpret_cast<char const*>(&fileSize), sizeof(fileSize));
         datFile.write(reinterpret_cast<char const*>(file.data.data()), file.data.size());
      }
      datFile.close();

   }
   else
   {
      error << "Invalid DAT file\n";
      exit(2);
   }
}
