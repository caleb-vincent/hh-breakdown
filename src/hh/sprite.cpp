//
// Copyright (c) 2021 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "sprite.hpp"

#include "color_palette.hpp"

#include <span>

using namespace hh;


template<>
std::unique_ptr<SpriteSheet> hh::CreateResource<SpriteSheet>(File const& file)
{
    return std::make_unique<SpriteSheet>(file);
}

const sf::Texture& SpriteSheet::operator[](size_t i) const
{
   if(ColorPalette::Get().Version() > m_paletteVersion)
   {
      m_paletteVersion = ColorPalette::Get().Version();
      m_sprites.clear();
      GenerateSprites();
   }
   return m_sprites[i];
}

void SpriteSheet::GenerateSprites() const
{
   size_t dataPos{ 0 };
   while( dataPos < m_datFile.data.size())
   {
      auto& sprite{ m_sprites.emplace_back() };

      auto width{ (m_datFile.data[dataPos + 1] << 8) | m_datFile.data[dataPos] };
      auto height{ (m_datFile.data[dataPos + 3] << 8) | m_datFile.data[dataPos + 2] };

      std::vector<uint8_t> pixels;
      pixels.reserve(width * height* 4);

      auto const& palette{ ColorPalette::Get() };
      for(auto& p : std::span(&m_datFile.data[dataPos + 4], width * height))
      {
         auto const& pixel = palette[p];
         pixels.push_back(pixel.r);
         pixels.push_back(pixel.g);
         pixels.push_back(pixel.b);
         pixels.push_back((p != 0) == 0 ? 0 : 255);
      }

      sprite.create(width, height);

      sprite.update(pixels.data());

      dataPos += (width * height) + 4;
   }
}

SpriteSheet::SpriteSheet(const File& datFile) :
   m_datFile{ datFile }
{
   GenerateSprites();
}
