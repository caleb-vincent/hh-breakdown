cmake_minimum_required(VERSION 3.10)

file(GLOB hh_SRC "*.?pp")

add_library(hh_lib
   ${hh_SRC})

set_property(TARGET hh_lib PROPERTY CXX_STANDARD 20)

target_compile_options(hh_lib PRIVATE "-Wall")
target_compile_options(hh_lib PRIVATE "-Wpedantic")
target_compile_options(hh_lib PRIVATE "-Wextra")
target_compile_options(hh_lib PRIVATE "-Werror")

target_link_libraries(hh_lib
  palletone)

target_include_directories(hh_lib INTERFACE
     ${CMAKE_CURRENT_SOURCE_DIR}
   )
