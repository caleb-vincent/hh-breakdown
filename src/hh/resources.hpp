#pragma once
//
// Copyright (c) 2021 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "dat.hpp"

#include <algorithm>
#include <any>
#include <memory>
#include <string>
#include <typeindex>
#include <unordered_map>

namespace hh
{
   template<class T>
   using Resource_t = T*;
   //It would be this, but the chaining of -> chokes up
   //using Resource_t = std::optional<ResourceWrapper_t<T>>;

   template<class T, typename... Args>
   extern std::unique_ptr<T> CreateResource(File const& datFile);

   template<class T>
   void SaveResource(std::any const&, File&);

   //! @todo Replace all the std::unique_ptr, std::shared_ptr and std::any nonsense with something better intened for this
   class Resources
   {
   public:
      explicit Resources(Dat& dat) :
         m_dat{ dat }
      {}

      template<class T>
      Resource_t<T const> Get(std::string const& name) const
      {
         auto iter{ m_resources.find(name) };
         if(iter != m_resources.end())
         {
            return std::any_cast<std::shared_ptr<T>>(iter->second).get();
         }
         else
         {
            if(auto iter{ std::find_if(m_dat.begin(), m_dat.end(), [&name](auto const& f){ return f.name == name; }) };
               iter != m_dat.end())
            {
               auto const p{ m_resources.emplace(std::make_pair(name, std::shared_ptr<T>(CreateResource<T>(*iter).release()))) };
               if(p.second)
               {
                  return std::any_cast<std::shared_ptr<T>>(p.first->second).get();
               }
            }
         }

         return {};
      }

      template<class T>
      Resource_t<T> Get(std::string const& name)
      {
         auto iter{ m_resources.find(name) };
         if(iter != m_resources.end())
         {
            return std::any_cast<std::shared_ptr<T>>(iter->second).get();
         }
         else
         {
            if(auto iter{ std::find_if(m_dat.begin(), m_dat.end(), [&name](auto const& f){ return f.name == name; }) };
               iter != m_dat.end())
            {
               auto const p{ m_resources.emplace(name, std::shared_ptr<T>(CreateResource<T>(*iter).release())) };
               if(p.second)
               {
                  // Since we're getting a non-const Resource, assume it will be modified and retrieve it's save method
                  if(auto const typeIndex{ std::type_index(typeid(std::shared_ptr<T>)) };
                     !m_saveFunctions.contains(typeIndex))
                  {
                     m_saveFunctions.emplace(typeIndex, &SaveResource<T>);
                  }
                  return std::any_cast<std::shared_ptr<T>>(p.first->second).get();
               }
            }
         }

         return {};
      }

      void Save();

   private:
      Dat& m_dat;

      mutable std::unordered_map<std::string, std::any> m_resources;
      mutable std::unordered_map<std::type_index, std::function<void(std::any, File&)>> m_saveFunctions;
   };

}
