//
// Copyright (c) 2018,2020-2021 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#include "map.hpp"

using namespace hh;

namespace
{
   auto constexpr MAP_ENTRY_DAT_SIZE{ sizeof(MapEntry::xPosition) + sizeof(MapEntry::yPosition) + sizeof(MapEntry::id) };
}

template<>
std::unique_ptr<Map> hh::CreateResource<Map>(File const& mapFile)
{
   auto map{ std::make_unique<Map>() };
   map->reserve(mapFile.data.size() / MAP_ENTRY_DAT_SIZE);

   for( auto dataPos{0u}; dataPos < mapFile.data.size(); dataPos += MAP_ENTRY_DAT_SIZE )
   {
      map->emplace_back(
         (mapFile.data[dataPos + 1] << 8) | mapFile.data[dataPos],
         mapFile.data[dataPos + 2],
         (mapFile.data[dataPos + 5] << 8) | mapFile.data[dataPos + 4] );
   }
   return map;
}

template<>
void hh::SaveResource<Map>(std::any const& anyMap, File& file)
{
   auto const& map{ std::any_cast<std::shared_ptr<Map>>(anyMap) };
   file.data.resize(map->size() * sizeof(uint16_t) * 3);
   size_t fileIndex{ 0u };
   for(auto const& entry : *map )
   {
      file.data[fileIndex++] = entry.xPosition & 0xFF;
      file.data[fileIndex++] = (entry.xPosition >> 8) & 0xFF;

      file.data[fileIndex++] = entry.id & 0xFF;
      file.data[fileIndex++] = (entry.id >> 8) & 0xFF;

      file.data[fileIndex++] = entry.yPosition & 0xFF;
      file.data[fileIndex++] = (entry.yPosition >> 8) & 0xFF;
   }
}
