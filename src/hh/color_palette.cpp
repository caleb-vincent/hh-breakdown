//
// Copyright (c) 2021 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#include "color_palette.hpp"

#include "color_256.hpp"

#include <algorithm>

using namespace hh;

ColorPalette ColorPalette::ms_colorPalette;

ColorPalette& ColorPalette::Get()
{
   return ms_colorPalette;
}

sf::Color const& ColorPalette::operator[](ColorPalette::Index_t indx) const
{
   return m_palette[indx];
}

sf::Color& ColorPalette::operator[](ColorPalette::Index_t indx)
{
   ++m_version;
   return m_palette[indx];
}

ColorPalette::ColorPalette()
{
   std::copy(COLOR_256_MODE.begin(), COLOR_256_MODE.end(), m_palette.begin());
}
