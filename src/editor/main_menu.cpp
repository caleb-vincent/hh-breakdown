//
// Copyright (c) 2018,2020-2021 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "main_menu.hpp"

#include <imgui/imgui.h>

#include <string>
#include <vector>

using namespace hh::editor;

MainMenu::MainMenu(WindowList_t const& windowList, WindowSelectCallbact_t windowCallback) :
   SelectWindow{ windowCallback },
   m_windowList{ windowList }
{}

void MainMenu::Frame()
{
   if(ImGui::BeginMainMenuBar())
   {
      
      if(ImGui::BeginMenu("File"))
      {
         if(ImGui::MenuItem("Import", nullptr, false, false))
         {}
         if(ImGui::MenuItem("Export", nullptr, false, false))
         {}
         
         ImGui::EndMenu();
      }
      if(ImGui::BeginMenu("Window", m_windowList.size() > 0))
      {
         for(auto const& window : m_windowList)
         {
            if(ImGui::MenuItem(window.c_str()))
            {
               SelectWindow(window);
            }
         }

         ImGui::EndMenu();
      }

#ifndef NDEBUG
      ImGui::Separator();
      ImGui::PushStyleColor(ImGuiCol_Text, ImGui::GetStyleColorVec4(ImGuiCol_TextDisabled));
      ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
      ImGui::PopStyleColor();
      
      ImGui::Separator();
      ImGui::MenuItem("Dear ImGui Demo", nullptr, &m_showDemoWindow);
#endif

      ImGui::EndMainMenuBar();
   }
}

bool MainMenu::ShowDemo() const
{
   return m_showDemoWindow;
}

