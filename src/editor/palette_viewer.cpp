//
// Copyright (c) 2021 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#include "palette_viewer.hpp"

#include "color_palette.hpp"

#include <imgui-SFML.h>

#include <imgui/imgui.h>

#include <cstring>

using namespace hh::editor;

namespace
{
auto constexpr INVALID_SELECTION{ std::numeric_limits<size_t >::max() };
auto constexpr PALETTE_ROOT_SIZE{ 16 };
auto constexpr PALETTE_ROOT_SIZEF{ (float)PALETTE_ROOT_SIZE };
auto const BLANK_TEXTURE{ []()
                          {
                             auto tex{ sf::Texture() };
                             uint8_t pixel[PALETTE_ROOT_SIZE * PALETTE_ROOT_SIZE * 4]{ 0 };
                             tex.create(PALETTE_ROOT_SIZE,PALETTE_ROOT_SIZE);
                             tex.update(&pixel[0]);
                             return tex;
                          }() };
}

PaletteViewer::PaletteViewer(Resources const& dat, std::vector<std::string> bitmaps) :
   m_res{ dat },
   m_windowName{ "Palette" },
   m_bitmaps{ bitmaps },
   m_foreground{ m_res, m_bitmaps, "Foreground", 144, 255 },
   m_background{ m_res, m_bitmaps, "Background", 0, 143 }
{
   std::sort(m_bitmaps.begin(), m_bitmaps.end());
}

void PaletteViewer::Frame()
{
   ImGui::Begin(m_windowName.c_str());

   m_foreground.Frame();
   m_background.Frame();

   static constexpr auto flags{ ImGuiTableFlags_Hideable | ImGuiTableFlags_SizingFixedFit };

   ImVec2 const outer_size(0.0f, ImGui::GetContentRegionAvail().y);
   if (ImGui::BeginTable("", PALETTE_ROOT_SIZE + 2, flags, outer_size))
   {
      static constexpr auto BACKGROUND_CUTOFF{ 144u };
      for (auto row{0u}; row < PALETTE_ROOT_SIZE; ++row)
      {
         ImGui::TableNextRow();
         ImGui::TableSetColumnIndex(0);
         auto const rowStart{ row * PALETTE_ROOT_SIZE };
         ImGui::Text("0x%0.2X", rowStart);
         for (auto col{0u}; col < PALETTE_ROOT_SIZE; ++col)
         {
            ImGui::TableSetColumnIndex(col + 1);
            auto const& paletteTex{ [&]()->sf::Texture const&
               {
                  if((row * PALETTE_ROOT_SIZE + col) > BACKGROUND_CUTOFF)
                  {
                     return m_foreground.bitmap ? m_foreground.bitmap->palette : BLANK_TEXTURE;
                  }
                  else
                  {
                     return m_background.bitmap ? m_background.bitmap->palette : BLANK_TEXTURE;
                  }
               }() };
            ImTextureID textureID = (ImTextureID)NULL;
            auto const glTextureId{ paletteTex.getNativeHandle() };
            std::memcpy(&textureID, &glTextureId, sizeof(glTextureId));

            ImVec2 const uv0(col / PALETTE_ROOT_SIZEF, row / PALETTE_ROOT_SIZEF);
            ImVec2 const uv1((col + 1) / PALETTE_ROOT_SIZEF, (row + 1) / PALETTE_ROOT_SIZEF);
            ImGui::Image(textureID, ImVec2(PALETTE_ROOT_SIZE, PALETTE_ROOT_SIZE), uv0, uv1);
         }

         ImGui::TableSetColumnIndex(PALETTE_ROOT_SIZE + 1);
         ImGui::Text("0x%0.2X", rowStart + PALETTE_ROOT_SIZE -1);
      }
      ImGui::EndTable();
   }
   ImGui::End();
}

PaletteViewer::PaletteSelector::PaletteSelector(hh::Resources const& dat, std::vector<std::string>& bitmaps, std::string name, uint8_t min, uint8_t max) :
   selected{ INVALID_SELECTION },
   m_res{ dat },
   m_bitmaps{ bitmaps },
   m_name{ name },
   m_min{ min },
   m_max{ max }
{}

void PaletteViewer::PaletteSelector::Frame()
{
   if (const char* combo_label{ (selected != INVALID_SELECTION) ? m_bitmaps[selected].c_str() : "" };
       ImGui::BeginCombo(m_name.c_str(), combo_label, 0))
   {
      for (auto n{0u}; n < m_bitmaps.size(); n++)
      {
         const bool is_selected = (selected == n);
         if (ImGui::Selectable(m_bitmaps[n].data(), is_selected))
         {
            if(n != selected)
            {
               selected = n;
               if(selected != INVALID_SELECTION)
               {
                  bitmap = m_res.Get<Bitmap>(m_bitmaps[selected]);
                  if(bitmap)
                  {
                     auto& palette{ ColorPalette::Get() };

                     auto const start{ m_min == 0 ? m_min + 1 : m_min };
                     std::transform(&bitmap->image.colors[start], &bitmap->image.colors[m_max + 1], &palette[start],
                           [](auto const& c)
                           {
                              return sf::Color(c.red, c.green, c.blue, 255);
                           });
                     if(m_min == 0)
                     {
                        palette[0] = sf::Color::Transparent;
                     }
                   }
                     else
                     {
                        bitmap = nullptr;
                     }
                  }
                  else
                  {
                     bitmap = nullptr;
                  }
               }
            }

            if (is_selected)
            {
               ImGui::SetItemDefaultFocus();
            }
         }
         ImGui::EndCombo();
      }
      ImGui::SameLine();

      static constexpr auto PREVIEW_SIZE{ 24.0 };
      ImGui::Image(bitmap ? bitmap->texture : BLANK_TEXTURE,
                   bitmap ? sf::Vector2f(bitmap->texture.getSize().x * (PREVIEW_SIZE / bitmap->texture.getSize().y), PREVIEW_SIZE) :
                            sf::Vector2f(PREVIEW_SIZE, PREVIEW_SIZE));
   }
