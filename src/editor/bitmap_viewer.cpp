//
// Copyright (c) 2021 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "bitmap_viewer.hpp"

#include "bitmap.hpp"
#include "color_palette.hpp"

#include <imgui/imgui.h>

#include <imgui-SFML.h>

using namespace hh::editor;

namespace
{
   auto constexpr INVALID_SELECTION{ std::numeric_limits<size_t >::max() };
}

BitmapViewer::BitmapViewer(hh::Resources const& res, std::string name, Bitmaps_t bitmaps) :
   m_resources{ res },
   m_windowName{ name },
   m_bitmaps{ bitmaps },
   m_selectedBitmap{ INVALID_SELECTION }
{
   std::sort(m_bitmaps.begin(), m_bitmaps.end());
}

void BitmapViewer::Frame()
{
   ImGui::Begin(m_windowName.c_str());

   bool bitmapChange{ false };

   if (const char* combo_label{ (m_selectedBitmap != INVALID_SELECTION) ? m_bitmaps[m_selectedBitmap].c_str() : "" };
       ImGui::BeginCombo("", combo_label, 0))
   {
      for (auto n{0u}; n < m_bitmaps.size(); n++)
      {
         const bool is_selected = (m_selectedBitmap == n);
         if (ImGui::Selectable(m_bitmaps[n].data(), is_selected))
         {
            if(n != m_selectedBitmap)
            {
               bitmapChange = true;
            }
            m_selectedBitmap = n;
         }

         if (is_selected)
         {
            ImGui::SetItemDefaultFocus();
         }
      }
      ImGui::EndCombo();
   }

   if(bitmapChange)
   {
      m_bitmap = m_resources.Get<Bitmap>(m_bitmaps[m_selectedBitmap]);
   }

   if(m_bitmap)
   {
      auto const textureSize{ m_bitmap->texture.getSize() };
      ImGui::Image(m_bitmap->texture, sf::Vector2f(textureSize.x * 2.0, textureSize.y * 2.0));
   }

   if(m_bitmap)
   {
       static constexpr auto flags{ ImGuiTableFlags_ScrollY |
                                   ImGuiTableFlags_RowBg |
                                   ImGuiTableFlags_Resizable |
                                   ImGuiTableFlags_Reorderable |
                                   ImGuiTableFlags_Hideable };

       ImVec2 const outer_size(0.0f, ImGui::GetContentRegionAvail().y);
       if (ImGui::BeginTable("Ranges", 5, flags, outer_size))
       {
           ImGui::TableSetupScrollFreeze(0, 1); // Make top row always visible
           ImGui::TableSetupColumn("Flags", ImGuiTableColumnFlags_None);
           ImGui::TableSetupColumn("High", ImGuiTableColumnFlags_None);
           ImGui::TableSetupColumn("Low", ImGuiTableColumnFlags_None);
           ImGui::TableSetupColumn("Rate", ImGuiTableColumnFlags_None);
           ImGui::TableSetupColumn("Time", ImGuiTableColumnFlags_None);
           ImGui::TableHeadersRow();

           ImGuiListClipper clipper(m_bitmap->image.ranges.size());
           while (clipper.Step())
           {
               for (auto row{clipper.DisplayStart}; row < clipper.DisplayEnd; row++)
               {
                   ImGui::TableNextRow();

                   ImGui::TableSetColumnIndex(0);
                   ImGui::Text("0x%0.4X", m_bitmap->image.ranges[row].flags);

                   ImGui::TableSetColumnIndex(1);
                   ImGui::Text("0x%0.2X", m_bitmap->image.ranges[row].high);

                   ImGui::TableSetColumnIndex(2);
                   ImGui::Text("0x%0.2X", m_bitmap->image.ranges[row].low);

                   ImGui::TableSetColumnIndex(3);
                   ImGui::Text("%d", m_bitmap->image.ranges[row].rate);

                   ImGui::TableSetColumnIndex(4);
                   ImGui::Text("%f", m_bitmap->image.ranges[row].time);
               }
           }
           ImGui::EndTable();
       }
   }

   ImGui::End();
}


const std::string& BitmapViewer::WindowName() const
{
   return m_windowName;
}
