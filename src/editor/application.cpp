//
// Copyright (c) 2021 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "application.hpp"

#include <imgui/imgui.h>

#include <imgui-SFML.h>

#include <SFML/System/Clock.hpp>
#include <SFML/Window/Event.hpp>

#include <algorithm>

using namespace hh::editor;


namespace
{

   static constexpr auto APPLICATION_TITLE{ "HH Editor" };
   static constexpr auto APPLICATION_WIDTH{ 1280 };
   static constexpr auto APPLICATION_HEIGHT{ 720 };

   std::vector<std::string> findBitmaps(hh::Dat const& dat)
   {
      std::vector<std::string> roads;
      for(auto& file : dat)
      {
         if(file.name.ends_with(".lbm"))
         {
            roads.push_back(file.name);
         }
      }
      return roads;
   }

   void ShowWindow(std::string const& windowName)
   {
      ImGui::SetWindowFocus(windowName.c_str());
   }
}


Application::Application() :
   window{ sf::VideoMode(APPLICATION_WIDTH, APPLICATION_HEIGHT), APPLICATION_TITLE },
   m_config{ Init() },
   m_dat{ readFile(m_config.datfile) },
   m_resources{ m_dat },
   m_bitmapViewer{ m_resources, "Bitmaps", findBitmaps(m_dat) },
   m_mapViewer{ m_resources, [this]()
                              {
                                 std::vector<std::string> maps;
                                 for(auto& file : m_dat)
                                 {
                                    if(file.name.ends_with(".map"))
                                    {
                                       maps.push_back(file.name);
                                    }
                                 }
                                 return maps;
                              }() },
   m_paletteViewer{ m_resources, findBitmaps(m_dat) },
   m_spriteViewer{ m_resources, [](Dat const& dat)
                                 {
                                    std::vector<std::string> sheets;
                                    for(auto& file : dat)
                                    {
                                       if(file.name.ends_with(".spr"))
                                       {
                                          sheets.push_back(file.name);
                                       }
                                    }
                                    return sheets;
                                 }(m_dat)},
   m_mainMenu(m_windowList, &ShowWindow)
{
   m_windowList.push_back(m_bitmapViewer.WindowName());
   m_windowList.push_back(m_mapViewer.WindowName());
   m_windowList.push_back(m_paletteViewer.WindowName());
   m_windowList.push_back(m_spriteViewer.WindowName());

   std::sort(m_windowList.begin(), m_windowList.end());
}

Application::~Application()
{
   // For now, save any changes as a backup file on exit
   m_resources.Save();
   writeFile(m_config.datfile + ".bak",  m_dat);
}

void Application::run()
{
   while (window.isOpen())
   {
      sf::Event event;
      while (window.pollEvent(event)) {
         ImGui::SFML::ProcessEvent(event);

         if (event.type == sf::Event::Closed) {
            window.close();
         }
      }

      ImGui::SFML::Update(window, deltaClock.restart());

      render();
      ImGui::EndFrame();
      window.clear();
      ImGui::SFML::Render(window);
      window.display();
   }
}

void Application::render()
{

   m_mainMenu.Frame();
   m_spriteViewer.Frame();
   m_bitmapViewer.Frame();
   m_mapViewer.Frame();
   m_paletteViewer.Frame();

   m_mapViewer.DoModal();

   if(m_mainMenu.ShowDemo())
   {
      ImGui::ShowDemoWindow();
   }

}

Config Application::Init()
{
   window.setFramerateLimit(60);
   ImGui::SFML::Init(window);
   return {"hh_editor.cnf"};
}
