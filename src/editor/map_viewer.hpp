#pragma once
//
// Copyright (c) 2018,2020-2021 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "dat.hpp"
#include "map.hpp"

#include <array>
#include <optional>
#include <string>
#include <vector>

namespace hh::editor
{
   class MapViewer
   {
   public:
      MapViewer(Resources& res, std::vector<std::string> maps);

      void Frame();

      void DoModal();

      const std::string& WindowName() const;

   private:
      void SwitchMap();
      void Save();

      Resources& m_res;
      std::string const m_windowName;

      size_t m_selectedMap;
      size_t m_nextSelectedMap;
      std::vector<std::string> m_maps;
      Resource_t<Map> m_map;

      std::vector<std::array<std::string, 3>> m_mapText;
      bool m_modifiedData;
   };
}
