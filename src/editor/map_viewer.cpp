//
// Copyright (c) 2018,2020-2021 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "map_viewer.hpp"

#include <imgui/imgui.h>
#include <imgui/imgui_internal.h>

#include <cstdlib>
#include <limits>

using namespace hh::editor;

namespace
{
   auto constexpr INVALID_SELECTION{ std::numeric_limits<size_t>::max() };
   auto constexpr SAVE_CONFIRM_POPUP{ "Modified Map Data" };
}

MapViewer::MapViewer(Resources& res, std::vector<std::string> maps):
   m_res{ res },
   m_windowName{ "Maps" },
   m_selectedMap{ INVALID_SELECTION },
   m_nextSelectedMap{ INVALID_SELECTION },
   m_maps{ maps },
   m_modifiedData{ false }
{
   std::sort(m_maps.begin(), m_maps.end());
}

void MapViewer::Frame()
{
   ImGui::Begin(m_windowName.c_str());

   const char* combo_label = m_selectedMap != INVALID_SELECTION ? m_maps[m_selectedMap].c_str() : "";  // Label to preview before opening the combo (technically could be anything)(
   if (ImGui::BeginCombo("", combo_label, 0))
   {
      for (auto n{0u}; n < m_maps.size(); n++)
      {
         const bool is_selected = (m_selectedMap == n);
         if (ImGui::Selectable(m_maps[n].data(), is_selected))
         {
            m_nextSelectedMap = n;
         }

         // Set the initial focus when opening the combo (scrolling + keyboard navigation focus)
         if (is_selected)
         {
            ImGui::SetItemDefaultFocus();
         }
      }
      ImGui::EndCombo();
   }

   ImGui::SameLine();
   if(!m_modifiedData)
   {
      ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
      ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
   }

   if(ImGui::Button("Save"))
   {
      Save();
   }
   else if(!m_modifiedData)
   {
      ImGui::PopItemFlag();
      ImGui::PopStyleVar();
   }

   if (m_mapText.size() > 0)
   {
      static constexpr auto flags{  ImGuiTableFlags_ScrollY |
                                    ImGuiTableFlags_RowBg |
                                    ImGuiTableFlags_Resizable |
                                    ImGuiTableFlags_Reorderable |
                                    ImGuiTableFlags_Hideable };

      ImVec2 const outer_size(0.0f, ImGui::GetContentRegionAvail().y);
      if (ImGui::BeginTable("map", 3, flags, outer_size))
      {
         ImGui::TableSetupScrollFreeze(0, 1); // Make top row always visible
         ImGui::TableSetupColumn("X", ImGuiTableColumnFlags_None);
         ImGui::TableSetupColumn("Y", ImGuiTableColumnFlags_None);
         ImGui::TableSetupColumn("ID", ImGuiTableColumnFlags_None);
         ImGui::TableHeadersRow();

         ImGuiListClipper clipper(m_mapText.size());
         char label[32]{ 0 };
         while (clipper.Step())
         {
            for (auto row{clipper.DisplayStart}; row < clipper.DisplayEnd; row++)
            {
               size_t column{0};
               ImGui::TableNextRow();

               snprintf(label, IM_ARRAYSIZE(label), "##label_%zX_%X", column, row);
               ImGui::TableSetColumnIndex(column);
               ImGui::PushItemWidth(-1);
               m_modifiedData |= ImGui::InputText(label, m_mapText[row][column].data(), m_mapText[row][column].size(), ImGuiInputTextFlags_CharsDecimal);
               ImGui::PopItemWidth();

               snprintf(label, IM_ARRAYSIZE(label), "##label_%zX_%X", ++column, row);
               ImGui::TableSetColumnIndex(column);
               ImGui::PushItemWidth(-1);
               m_modifiedData |= ImGui::InputText(label, m_mapText[row][column].data(), m_mapText[row][column].size(), ImGuiInputTextFlags_CharsDecimal);
               ImGui::PopItemWidth();

               snprintf(label, IM_ARRAYSIZE(label), "##label_%zX_%X", ++column, row);
               ImGui::TableSetColumnIndex(column);
               ImGui::PushItemWidth(-1);
               m_modifiedData |= ImGui::InputText(label, m_mapText[row][column].data(), m_mapText[row][column].size(), ImGuiInputTextFlags_CharsUppercase | ImGuiInputTextFlags_CharsHexadecimal);
               ImGui::PopItemWidth();
            }
         }
         ImGui::EndTable();
      }
   }
   ImGui::End();


   if(m_nextSelectedMap != m_selectedMap)
   {
      if(m_modifiedData)
      {
         ImGui::OpenPopup(SAVE_CONFIRM_POPUP);
      }
      else
      {
         SwitchMap();
      }
   }
}

void MapViewer::DoModal()
{
   ImVec2 center = ImGui::GetMainViewport()->GetCenter();
   ImGui::SetNextWindowPos(center, ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));

   if(ImGui::BeginPopupModal(SAVE_CONFIRM_POPUP, nullptr, ImGuiWindowFlags_AlwaysAutoResize))
   {
      ImGui::Separator();

      if (ImGui::Button("Save", ImVec2(120, 0)))
      {
         Save();
         ImGui::CloseCurrentPopup();
         SwitchMap();
      }
      ImGui::SetItemDefaultFocus();
      ImGui::SameLine();
      if (ImGui::Button("Discard", ImVec2(120, 0)))
      {
         ImGui::CloseCurrentPopup();
         SwitchMap();
      }
      ImGui::EndPopup();
   }
}


const std::string& MapViewer::WindowName() const
{
   return m_windowName;
}

void MapViewer::SwitchMap()
{
   m_selectedMap = m_nextSelectedMap;
   m_map = m_res.Get<Map>(m_maps[m_selectedMap]);
   m_mapText.resize(m_map->size());
   for(auto i{0u}; i < m_map->size(); ++i)
   {
      auto constexpr BUF_SIZE{32u};
      //! @todo at least use constants, or maybe a struct instead of std::array
      m_mapText[i][0].resize(BUF_SIZE);
      m_mapText[i][1].resize(BUF_SIZE);
      m_mapText[i][2].resize(BUF_SIZE);

      snprintf(m_mapText[i][0].data(), BUF_SIZE -1, "%u", (*m_map)[i].xPosition);
      snprintf(m_mapText[i][1].data(), BUF_SIZE -1, "%u", (*m_map)[i].yPosition);
      snprintf(m_mapText[i][2].data(), BUF_SIZE -1, "0x%X", (*m_map)[i].id);
   }
}

void MapViewer::Save()
{
   for(auto i{0u}; i < m_mapText.size(); ++i)
   {
      IM_ASSERT(m_map->size() ==  m_mapText.size());
      (*m_map)[i].xPosition = static_cast<uint16_t>(atoi(m_mapText[i][0].c_str()));
      (*m_map)[i].yPosition = static_cast<uint16_t>(atoi(m_mapText[i][1].c_str()));
      auto id{ m_mapText[i][2] };
      if(id.starts_with("0x"))
      {
         if(id.size() > 2)
         {
            id = id.substr(2);
         }
         else
         {
            id.clear();
         }
      }
      (*m_map)[i].id = static_cast<uint16_t>(std::strtoul(id.c_str(), nullptr, 16));
   }
   m_modifiedData = false;
   //! @todo The resource Manager needs to save back to the DAT
}
