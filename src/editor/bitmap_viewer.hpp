#pragma once
//
// Copyright (c) 2021 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "resources.hpp"

#include "bitmap.hpp"
#include "resources.hpp"

#include <optional>

namespace hh::editor
{
   class BitmapViewer
   {
   public:
      using Bitmaps_t = std::vector<std::string> ;

      BitmapViewer(Resources const& resources, std::string name, Bitmaps_t bitmaps);

      void Frame();

      const std::string& WindowName() const;

   private:
      Resources const& m_resources;
      std::string const m_windowName;

      std::vector<std::string> m_bitmaps;

      size_t m_selectedBitmap;
      Resource_t<Bitmap const> m_bitmap;
   };
}
