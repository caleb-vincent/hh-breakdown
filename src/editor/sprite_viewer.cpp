//
// Copyright (c) 2021 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "sprite_viewer.hpp"

#include <imgui/imgui.h>

#include <imgui-SFML.h>

#include <limits>

using namespace hh::editor;

namespace
{
   auto constexpr INVALID_SELECTION{ std::numeric_limits<size_t >::max() };
}

SpriteViewer::SpriteViewer(Resources const& res, std::vector<std::string> spriteSheets) :
   m_resources{ res },
   m_windowName{ "Sprites" },
   m_spriteSheets{ spriteSheets },
   m_sprites{ nullptr },
   m_selectedSpriteSheet{ INVALID_SELECTION },
   m_selectedSprite{ 0 }
{
   std::sort(m_spriteSheets.begin(), m_spriteSheets.end());
}

void SpriteViewer::Frame()
{
   ImGui::Begin(m_windowName.c_str());

   bool spriteSheetChange{ false };
   const char* combo_label = m_selectedSpriteSheet != INVALID_SELECTION ? m_spriteSheets[m_selectedSpriteSheet].c_str() : "";  // Label to preview before opening the combo (technically could be anything)(
   if (ImGui::BeginCombo("", combo_label, 0))
   {
      for (auto n{0u}; n < m_spriteSheets.size(); n++)
      {
         const bool is_selected = ( m_selectedSpriteSheet == n);
         if (ImGui::Selectable(m_spriteSheets[n].data(), is_selected))
         {
            m_selectedSpriteSheet = n;
            spriteSheetChange = true;
         }

         // Set the initial focus when opening the combo (scrolling + keyboard navigation focus)
         if (is_selected)
         {
            ImGui::SetItemDefaultFocus();
         }
      }
      ImGui::EndCombo();
   }

   if(spriteSheetChange)
   {
      m_selectedSprite = 0;
   }

   ImGui::SameLine();
   {
      float spacing = ImGui::GetStyle().ItemInnerSpacing.x;
      ImGui::PushButtonRepeat(true);
      if (ImGui::ArrowButton("##left", ImGuiDir_Left) && m_selectedSprite > 0)
      {
         spriteSheetChange = true;
         m_selectedSprite--;
      }
      ImGui::SameLine(0.0f, spacing);
      ImGui::Text("%u", static_cast<uint32_t>(m_selectedSprite));
      ImGui::SameLine(0.0f, spacing);
      if (ImGui::ArrowButton("##right", ImGuiDir_Right) && m_sprites && (m_selectedSprite < m_sprites->Size() - 1))
      {
         spriteSheetChange = true;
         m_selectedSprite++;
      }
      ImGui::PopButtonRepeat();
   }

   if(spriteSheetChange)
   {
      m_sprites = m_resources.Get<SpriteSheet>(m_spriteSheets[m_selectedSpriteSheet]);
   }

   if(m_sprites && m_sprites->Size())
   {
      auto const& spriteSize{ (*m_sprites)[m_selectedSprite].getSize() };
      ImGui::Text("%u x %u", spriteSize.x, spriteSize.y);
      ImGui::Image((*m_sprites)[m_selectedSprite], sf::Vector2f(spriteSize.x * 4.0, spriteSize.y * 4.0));
   }

   ImGui::End();
}

const std::string& SpriteViewer::WindowName() const
{
   return m_windowName;
}

