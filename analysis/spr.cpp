//
// Copyright (c) 2021 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include <filesystem>
#include <fstream>
#include <iostream>
#include <iterator>
#include <vector>

namespace fs = std::filesystem;

auto& info = std::cout;
auto& warn = std::clog;
auto& error = std::cerr;

std::vector<uint8_t> ReadFile(fs::path const& filename)
{
   // open the file:
   std::ifstream file(filename, std::ios::binary);

   if(!file.good())
   {
      error << "Can't open file" << filename.string() << "\n";
      exit(2);
   }

   file.unsetf(std::ios::skipws);

   std::streampos fileSize;

   file.seekg(0, std::ios::end);
   fileSize = file.tellg();
   file.seekg(0, std::ios::beg);

   std::vector<uint8_t> vec;
   vec.reserve(fileSize);

   vec.insert(vec.begin(),
              std::istream_iterator<uint8_t>(file),
              std::istream_iterator<uint8_t>());

   return vec;
}


size_t WriteSprite(std::ofstream& spriteFile, uint8_t const*const data, size_t const size)
{
      size_t spriteSize{0};

      auto const xSize{*reinterpret_cast<uint16_t const*>(&data[0])}, ySize{*reinterpret_cast<uint16_t const*>(&data[2])};

      spriteFile << "P2\n";
      spriteFile << xSize << " " << ySize << "\n";
      spriteFile << "127\n";
      info << "Sprite is " << std::dec << xSize << "x" << ySize << "\n";\
      spriteSize = xSize * ySize + 4;
      for(auto dataPos{4u}; dataPos < spriteSize; ++dataPos)
      {
         if(dataPos >= size)
         {
            error << "Inconsistent Sprite size\n";
         }
         spriteFile << std::dec << static_cast<uint32_t>(0x7Fu & data[dataPos]) << "\n";
      }
      return spriteSize;
}

void WriteSprites(std::vector<uint8_t> const& file, fs::path const& inputFileName)
{
   auto spritePosition{0u};
   auto spriteNum{0u};
   while(spritePosition < file.size())
   {
      std::stringstream spriteFileName;
      spriteFileName << (inputFileName.parent_path() / inputFileName.stem()).string() << spriteNum << ".pgm";
      std::ofstream outFile( spriteFileName.str() , std::ios::binary);
      info << "Writing " << spriteFileName.str() << "\n";
      if(outFile.good())
      {
         spritePosition += WriteSprite(outFile, &file[spritePosition], file.size() - spritePosition);
         outFile.close();
      }
      ++spriteNum;
}
}


int main(int argc,char* argv[])
{
   if(argc < 2)
   {
      error << "Incorrect number of arguments\n spr <spritesheetname>\n";
      return 1;
   }

   auto dataFile{ ReadFile(argv[1]) };
   if(dataFile.size() == 0)
   {
      error << "Empty file" << argv[1] << "\n";
      return 3;
   }

   WriteSprites(dataFile, argv[1]);

   return 0;
}
