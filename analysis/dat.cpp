//
// Copyright (c) 2021 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include "dat.hpp"


#include <filesystem>
#include <fstream>
#include <iostream>


auto& info = std::cout;
auto& warn = std::clog;
auto& error = std::cerr;


std::ifstream& operator>>(std::ifstream& datFile, IndexEntry& entry)
{
   datFile.read(reinterpret_cast<char*>(&entry.filePosition), sizeof(entry.filePosition));
   datFile.read(entry.fileName, sizeof(entry.fileName));
   info << "File '"<< entry.fileName << "' at 0x" << std::hex << entry.filePosition << "\n";
   return datFile;
}


std::ifstream& operator>>(std::ifstream& datFile, File& file)
{
   datFile.read(reinterpret_cast<char*>(&file.size), sizeof(file.size));
   file.data = new uint8_t[file.size];
   datFile.read(reinterpret_cast<char*>(file.data), file.size);
   return datFile;
}



std::ifstream& operator>>(std::ifstream& datFile, Index& index)
{
   datFile.read(reinterpret_cast<char*>(&index.endPosition), sizeof(index.endPosition));
   info << "Index Size: " << index.endPosition + 1 << "\n";
   auto const entryCount = (index.endPosition - sizeof(index.endPosition)) / (sizeof(IndexEntry::filePosition) + sizeof(IndexEntry::fileName));
   info << "Index entries: " << entryCount << "\n";
   index.entries = new IndexEntry[entryCount];
   for(auto i{0u}; i < entryCount; ++i)
   {
      datFile >> index.entries[i];

   }
   datFile.seekg(index.endPosition+2);
   return datFile;
}



int main()
{
   std::ifstream datFile("1.DAT", std::ios::binary);
   if(datFile.good())
   {
      Dat dat;
      datFile >> dat.index;
      auto const fileCount = (dat.index.endPosition - sizeof(dat.index.endPosition)) / (sizeof(IndexEntry::filePosition) + sizeof(IndexEntry::fileName));
      dat.files = new File[fileCount];
      for(auto i{0u}; i < fileCount; ++i)
      {
         datFile.seekg(dat.index.entries[i].filePosition);
         datFile >> dat.files[i];
         info << "Read " << dat.index.entries[i].fileName << " " << std::hex << dat.files[i].size << " bytes\n";
      }
      for(auto i{0u}; i < fileCount; ++i)
      {
         using namespace std::string_literals;
         std::ofstream outFile("output/"s + dat.index.entries[i].fileName, std::ios::binary);
         if(outFile.good())
         {
            outFile.write(reinterpret_cast<char*>(dat.files[i].data), dat.files[i].size);
            info << "Wrote " << dat.index.entries[i].fileName << " " << std::hex << dat.files[i].size << " bytes\n";
            outFile.close();
         }
         else
         {
            error << "Cannot write to " << dat.index.entries[i].fileName << "\n";
            exit(2);
         }
      }

      datFile.close();

   }
   else
   {
      error << "Invalid DAT file\n";
      exit(1);
   }
}
