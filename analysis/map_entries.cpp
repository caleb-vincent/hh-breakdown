//
// Copyright (c) 2021 Caleb Vincent
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include <array>
#include <bitset>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
#include <sstream>

auto& info = std::cout;
auto& warn = std::clog;
auto& error = std::cerr;


int main(int argc,char* argv[])
{
   if(argc < 2)
   {
      error << "Incorrect number of arguments\n map <mapname>\n";
      return 1;
   }

   std::ifstream file(argv[1], std::ios::binary);
   std::ofstream outFile(std::string(argv[1]) + ".txt");

   std::array<uint8_t, 6> entry;

   //grab entries
   while( outFile.good() && file.good() && !file.eof() )
   {
      // 6 bytes at a time
      file.read(reinterpret_cast<char*>(&entry[0]), sizeof(entry));

      // until we get anything
      if( file.gcount() < static_cast<decltype(file.gcount())>(sizeof(entry)) )
      {
         // that we don't care about
         break;
      }

      std::stringstream binaryBuffer;
      for(auto const& byte : entry)
      {
         outFile << std::hex << std::setfill ('0') << std::setw (2) << std::uppercase << (int)byte;
         binaryBuffer << std::bitset<8>(byte);
      }
      outFile << "\t\t" << binaryBuffer.str() << "\n";
   }

   return 0;
}
