cmake_minimum_required(VERSION 3.10)
project(imgui)

set(OpenGL_GL_PREFERENCE "LEGACY")

find_package(OpenGL REQUIRED)

add_library(imgui INTERFACE
)

target_include_directories(imgui INTERFACE
  ${CMAKE_CURRENT_SOURCE_DIR}
)




